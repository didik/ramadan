/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.co.firzil.ramadan.service.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import id.co.firzil.ramadan.app.Const;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.FLog;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {

            GcmPreference gcm = new GcmPreference(this);
            if(! gcm.isRegisteredInServer()) {  //jika gcm belum di register ke server
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(Const.SENDER_ID,
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                FLog.d(TAG, "GCM Registration Token: " + token);

                Me.getInstance().setData(Me.GCM, token);

                new UpdateGcm().update(this);
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);

        }
    }

}

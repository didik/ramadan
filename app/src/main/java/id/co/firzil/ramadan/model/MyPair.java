package id.co.firzil.ramadan.model;

import android.view.View;

/**
 * Created by didik on 01/06/16.
 * My Pair
 */
public class MyPair {
    private View view;
    private String name;

    public MyPair(View view, String name) {
        this.view = view;
        this.name = name;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

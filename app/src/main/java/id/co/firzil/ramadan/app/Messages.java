package id.co.firzil.ramadan.app;

/**
 * Created by didik on 25/04/16.
 * List message
 */
public class Messages {
    // Toast
    public static final String NO_INTERNET = "You are not connected to the internet!";
    public static final String FAILED_TO_CONNECT = "Failed to connect to the server!";

    // Progress dialog
    public static final String REGISTERING = "Registering...";
    public static final String LOGGING_IN = "Logging In...";
    public static final String UPDATING_PROFILE = "Updating profile...";
    public static final String UPDATING_PASS = "Updating password...";

    public static final String REGISTER_FAILED = "The email or phone has already been taken.";
    public static final String LOGIN_FAILED = "Email or password you entered is incorrect. Please try again.";

    //public static final String GPS_OFF = "Please turn on Location Services. For the best experience, please set it to High Accuracy.";
    public static final String GPS_OFF = "Mohon aktifkan GPS untuk mendapatkan layanan terbaik.";
}

package id.co.firzil.ramadan.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import id.co.firzil.ramadan.app.App;
import id.co.firzil.ramadan.app.Const;
import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.model.Place;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by didik on 26/05/16.
 * Place place
 */
public class NearbyPlace {
    private static final String TAG = "nearbyplace";

    public interface Callbacks {
        public void onSuccess(ArrayList<Place> places);

        public void onError();
    }

    private static String getUrl(double lat, double lng) {
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=";
        String latlng = lat + "," + lng;
        String key = "&radius=5000&name=masjid&key=" + Const.GOOGLE_KEY;
        return url + latlng + key;
    }

    public static void get(final Context context, double lat, double lng, final Callbacks callback) {
        String url = getUrl(lat, lng);
        FLog.d(TAG, url);
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Mencari masjid terdekat");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Request request = new Request.Builder()
                .url(url)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        callback.onError();
                    }
                });
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                final String body = response.body().string();
                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            try {
                                ArrayList<Place> places = new ArrayList<>();
                                JSONObject data = new JSONObject(body);
                                JSONArray res = data.getJSONArray("results");
                                for (int i = 0; i < res.length(); i++) {
                                    JSONObject objRes = res.getJSONObject(i);
                                    Place place = new Place();

                                    place.setName(objRes.getString("name"));
                                    place.setPlaceId(objRes.getString("place_id"));
                                    place.setVicinity(objRes.getString("vicinity"));

                                    JSONObject location = objRes.getJSONObject("geometry").getJSONObject("location");
                                    place.setLat(location.getDouble("lat"));
                                    place.setLng(location.getDouble("lng"));

                                    places.add(place);
                                }
                                callback.onSuccess(places);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else callback.onError();
                    }
                });
            }
        });
    }
}

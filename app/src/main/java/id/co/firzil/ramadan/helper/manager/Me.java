package id.co.firzil.ramadan.helper.manager;

import android.content.Context;
import android.content.SharedPreferences;

import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.model.User;


/**
 * Created by didik on 25/04/16.
 * Me
 */
public class Me {
    private static String TAG = Me.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    private static Me me;

    // Shared preferences file name
    private static final String PREF_NAME = "Me";

    public static final String MWL = "mwl", EGYPT = "egypt", ISNA = "isna";

    public static final String
            ISLOGGEDIN = "isLoggedIn",
            NAME = "name",
            EMAIL = "email",
            ID = "id",
            API_TOKEN = "apiToken",
            GCM = "gcm",
            LAT = "lat",
            LNG = "lng",
            PHONE = "phone",
            REGISTERED = "registered",
            CURR_LAT = "curr_lat",
            CURR_LNG = "curr_lng",
            LOCATION = "location",
            PRAY_METHOD = "pray_method",
            NOTIF_SHOLAT = "sholat",
            NOtiF_BERITA = "berita";


    public Me(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static synchronized void init(Context context) {
        me = new Me(context);
    }

    public static synchronized Me getInstance() {
        return me;
    }

    public void setLogin(User user) {
        editor.putBoolean(ISLOGGEDIN, true);
        editor.putInt(ID, user.getId());
        editor.putString(NAME, user.getName());
        editor.putString(EMAIL, user.getEmail());
        editor.putString(API_TOKEN, user.getApiToken());
        editor.putString(GCM, user.getGcmId());
        editor.putString(PHONE, user.getPhone());
        editor.putString(REGISTERED, user.getRegistered());

        editor.commit();
    }

    public String getPrayMethod() {
        return pref.getString(PRAY_METHOD, EGYPT);
    }

    public void setPrayMethod(String data) {

        editor.putString(PRAY_METHOD, data);

        // commit changes
        editor.commit();
    }

    public String getData(String key) {
        return pref.getString(key, "");
    }

    public boolean getBooleanData(String key) {
        return pref.getBoolean(key, true);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(ISLOGGEDIN, false);
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        editor.putBoolean(ISLOGGEDIN, isLoggedIn);
        editor.commit();
    }

    public void setData(String key, String data) {

        editor.putString(key, data);

        // commit changes
        editor.commit();
    }

    public void setData(String key, boolean data) {

        editor.putBoolean(key, data);

        // commit changes
        editor.commit();
    }

    public User getUser() {
        User user = new User();
        user.setId(pref.getInt(ID, 0));
        user.setName(pref.getString(NAME, ""));
        user.setEmail(pref.getString(EMAIL, ""));
        user.setApiToken(pref.getString(API_TOKEN, ""));
        user.setGcmId(pref.getString(GCM, ""));
        user.setPhone(pref.getString(PHONE, ""));
        user.setRegistered(pref.getString(REGISTERED, ""));

        return user;
    }

    public void clearData() {
        FLog.d(TAG, "clear data");
        editor.clear();
        editor.commit();
    }
}

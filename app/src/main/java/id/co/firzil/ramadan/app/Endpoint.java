package id.co.firzil.ramadan.app;

/**
 * Created by didik on 25/04/16.
 * Endpoint
 */
public class Endpoint {
    // Base
    public static final String BASE_URL = "http://bimbinganramadan.mlogg.com/api/";

    public static final String AUTH = BASE_URL + "auth/";
    public static final String LOGIN = AUTH + "login";
    public static final String REGISTER = AUTH + "register";
    public static final String SET_GCM = BASE_URL + "user/set-gcm";

    public static final String ARTICLE = BASE_URL + "articles";
    public static final String CATEGORY = BASE_URL + "categories";
    public static final String ARTICLE_ALL = BASE_URL + "articles/all";
    public static final String DONATION = BASE_URL + "donations";
    public static final String BANNER = BASE_URL + "banners";
}

package id.co.firzil.ramadan.feature.adzan;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.helper.Prayer;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.prayer.PrayerTime;
import id.co.firzil.ramadan.helper.prayer.PrayerTimes;
import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.helper.utils.FUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdzanFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.tv_hour) TextView tvHour;
    @BindView(R.id.tv_minute) TextView tvMinute;
    @BindView(R.id.tv_address) TextView tvAddress;
    @BindView(R.id.tv_date) TextView tvDate;
    @BindView(R.id.iv_cal) ImageView ivCal;
    @BindView(R.id.tv_method) TextView tvMethod;
    @BindView(R.id.tv_fajr) TextView tvFajr;
    @BindView(R.id.tv_sunrise) TextView tvSunrise;
    @BindView(R.id.tv_dhuhr) TextView tvDhuhr;
    @BindView(R.id.tv_asr) TextView tvAsr;
    @BindView(R.id.tv_maghrib) TextView tvMaghrib;
    @BindView(R.id.tv_isha) TextView tvIsha;

    private Me me = Me.getInstance();

    public AdzanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adzan, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(false);

        tvAddress.setText(me.getData(Me.LOCATION));
        showPrayerTime(new Date());
    }

    @Override
    public void onResume() {
        super.onResume();
        initNearbyPray();

        String method = me.getPrayMethod();
        if (method.equalsIgnoreCase(Me.MWL)) tvMethod.setText("Muslim World League (MWL)");
        else if (method.equalsIgnoreCase(Me.ISNA)) tvMethod.setText("Islamic Society of North America");
        else tvMethod.setText(getString(R.string.text_depag));
    }

    private void initNearbyPray() {
        PrayerTime near = Prayer.getNearPray();
        //PrayerTime near = Prayer.getFajrNextDay();
        long nearMilis = Prayer.ptToMilis(near);
        long diff = nearMilis - new Date().getTime();

        long hour = TimeUnit.MILLISECONDS.toHours(diff);
        long min = TimeUnit.MILLISECONDS.toMinutes(diff) -
                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.
                        toHours(diff));

        tvHour.setText(String.valueOf(hour));
        tvMinute.setText(String.valueOf(min));
    }

    private void showPrayerTime(Date date) {
        tvDate.setText(FUtils.dateFormatter(date.getTime(), FUtils.DATE_HUMAN_FORMAT_DAY));

        PrayerTimes prayerTimes = Prayer.getPrayerTimes(date);
        FLog.d("isipt", prayerTimes.get(0).getDate().toString());

        tvFajr.setText(prayerTimes.get(0).getTimes());
        tvSunrise.setText(prayerTimes.get(1).getTimes());
        tvDhuhr.setText(prayerTimes.get(2).getTimes());
        tvAsr.setText(prayerTimes.get(3).getTimes());
        tvMaghrib.setText(prayerTimes.get(4).getTimes());
        tvIsha.setText(prayerTimes.get(5).getTimes());
    }


    @OnClick({R.id.btn_nearby_mosque, R.id.iv_cal})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_nearby_mosque:
                startFragment(new NearbyFragment());
                break;
            case R.id.iv_cal:
                showCalendar();
                break;
        }
    }

    private void showCalendar() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        //dpd.setAccentColor(Color.parseColor("#009688"));
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        showPrayerTime(calendar.getTime());
    }
}

package id.co.firzil.ramadan.helper;

import android.location.Address;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import id.co.firzil.ramadan.app.App;
import id.co.firzil.ramadan.app.Const;
import id.co.firzil.ramadan.helper.utils.FLog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by didik on 27/05/16.
 * Geocode
 */
public class Geocoding {
    public interface Callbacks {
        public void onSuccess(Address address);

        public void onError();
    }

    private static String getGeocodeUrl(LatLng mlatLng) {

        // Latlong
        String latlong = "latlng=" + mlatLng.latitude + "," + mlatLng.longitude;

        // Building the parameters to the web service
        String parameters = latlong + "&key=" + Const.GOOGLE_KEY;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/geocode/" + output + "?" + parameters;


        return url;
    }

    public static void getAddress(final LatLng latLng, final Callbacks callbacks) {
        Request request = new Request.Builder()
                .url(getGeocodeUrl(latLng))
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callbacks.onError();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String body = response.body().string();
                FLog.d("isilocation", body);
                if (response.isSuccessful()) {
                    try {
                        JSONObject res = new JSONObject(body);
                        String address, locality = null, adminLv2 = null;
                        Address mAddress = new Address(Locale.getDefault());
                        mAddress.setLatitude(latLng.latitude);
                        mAddress.setLongitude(latLng.longitude);

                        JSONArray results = res.getJSONArray("results");
                        JSONObject selectedRes = results.getJSONObject(0);
                        address = selectedRes.getString("formatted_address");
                        mAddress.setAddressLine(0, address);
                        JSONArray addressComponent = selectedRes.getJSONArray("address_components");

                        for (int i = 0; i < addressComponent.length(); i++) {
                            JSONObject objComp = addressComponent.getJSONObject(i);
                            JSONArray types = objComp.getJSONArray("types");
                            String checkedType = types.getString(0);

                            if (checkedType.equalsIgnoreCase("administrative_area_level_2"))
                                adminLv2 = objComp.getString("long_name");

                            if (checkedType.equalsIgnoreCase("administrative_area_level_3")) {
                                locality = objComp.getString("long_name");
                                mAddress.setLocality(locality);
                            }
                        }

                        if (locality == null) mAddress.setLocality(adminLv2);

                        callbacks.onSuccess(mAddress);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        callbacks.onError();
                    }
                } else callbacks.onError();
            }
        });
    }
}

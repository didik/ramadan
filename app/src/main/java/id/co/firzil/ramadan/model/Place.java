package id.co.firzil.ramadan.model;

import java.io.Serializable;

/**
 * Created by didik on 26/05/16.
 * Place
 */
public class Place implements Serializable{
    private String placeId;
    private String name;
    private double lat;
    private double lng;
    private String vicinity;

    public Place() {
    }

    public Place(String placeId, String name, double lat, double lng, String vicinity) {
        this.placeId = placeId;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.vicinity = vicinity;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    @Override
    public String toString() {
        return "Place{" +
                "placeId='" + placeId + '\'' +
                ", name='" + name + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", vicinity='" + vicinity + '\'' +
                '}';
    }
}

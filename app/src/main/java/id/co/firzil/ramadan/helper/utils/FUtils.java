package id.co.firzil.ramadan.helper.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import id.co.firzil.ramadan.app.Messages;
import id.co.firzil.ramadan.model.Friend;


/**
 * Created by didik on 02/03/16.
 * Helper untuk fungsi-fungsi yang sering digunakan
 */
public class FUtils {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_DAY = "dd";
    public static final String DATE_HUMAN_FORMAT = "dd MMMM yyyy";
    public static final String DATE_HUMAN_FORMAT_MONTH = "MMMM, yyyy";
    public static final String DATE_HUMAN_FORMAT_DAY = "EEEE, dd MMMM yyyy";
    public static final String DATE_HUMAN_FORMAT_FULL = "dd MMMM yyyy - HH:mm";
    public static final String TIME_FORMAT_FULL = "HH:mm:ss";
    public static Locale locale = new Locale("in", "ID");

    //======================================================================
    // Network & Location Helper
    // 1. isNetworkConnected (Cek koneksi device)
    // 2. isLocationEnabled (Cek apakah GPS sudah aktif)
    //======================================================================

    /**
     * Check network connection
     *
     * @param context Context that you want to check the connection
     * @return true if device connected to the network
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    public static ArrayList<Friend> getNumber(ContentResolver cr) {
        ArrayList<Friend> friends = new ArrayList<>();
        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        // use the cursor to access the contacts
        assert phones != null;
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            // get display name
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            // get phone number
            System.out.println(".................." + phoneNumber);
            friends.add(new Friend(name, phoneNumber));
        }

        phones.close();
        return friends;
    }

    /**
     * Cek apakah GPS / Location Services sudah diaktifkan
     * Jika belum, maka user diarahkan ke Setting untuk mengaktifkannya.
     *
     * @param context Centext dimana kita ingin mengecek GPS
     */
    public static void isLocationEnabled(final Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            FLog.d("Location", ex.toString());
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            FLog.d("Location", ex.toString());
        }

        if (!gps_enabled && !network_enabled) {
            final MaterialDialog materialDialog = new MaterialDialog.Builder(context)
                    .title("GPS is off!")
                    .negativeText("Cancel")
                    .positiveText("Got It")
                    .content(Messages.GPS_OFF)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            context.startActivity(myIntent);
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .build();
            materialDialog.show();
        }
    }

    public static boolean isGpsActive(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            FLog.d("Location", ex.toString());
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            FLog.d("Location", ex.toString());
        }

        return gps_enabled && network_enabled;
    }

    //======================================================================
    // Date Helper
    // 1. convertStringToLong (konversi string ke format Date)
    // 2. convertToRelative (Konversi waktu ke relative format (2 jam yang lalu))
    // 3. dateFormatter (Ubah format tanggal ke beberapa bentuk)
    // 4. timeFormatter (Ubah format waktu menjadi hh:mm)
    // 5. getDateTime (Mendapatkan tanggal dan jam sekarang)
    // 6. getDateNow (Mendapatkan tanggal sekarang)
    // 7. getTimeNow (Mendapatkan jam sekarang)
    //======================================================================

    /**
     * Konversi String ke dalam bentuk objek Date
     *
     * @param date string yang akan dikonversi
     * @return Date object
     */
    public static long convertStringToLong(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return convertedDate.getTime();

    }

    public static long convertStringToLong(String date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                format, Locale.getDefault());
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return convertedDate.getTime();

    }


    /**
     * Konversi waktu ke relative format (2 jam yang lalu)
     *
     * @param date Waktu yang akan dikonversi
     * @return waktu yang sudah diformat
     */
    public static String convertToRelative(String date) {
        long now = System.currentTimeMillis();
        long time = convertStringToLong(date);

        String relativeFormat = (String) DateUtils.getRelativeTimeSpanString(time, now, DateUtils.SECOND_IN_MILLIS);

        return relativeFormat;
    }

    /**
     * Ubah format string ke dalam bentuk waktu yang diinginkan
     * Tipe :
     * 1. Full format (2016-01-15 22:00:00) -  DATE_FORMAT_FULL
     * 2. Human format (15 Januari 2016) - DATE_HUMAN_FORMAT
     * 3. Human format full (15 Januari 2016 22:00) - DATE_HUMAN_FORMAT_FULL
     *
     * @param date String Date yang akan diformat
     * @return tanggal yang sudah diformat (i.e. 2016-01-05 05:16:20)
     */
    public static String dateFormatter(String date, String formatType) {
        Date convertedDate = new Date(convertStringToLong(date));

        SimpleDateFormat format = new SimpleDateFormat(formatType, locale);

        return format.format(convertedDate);
    }

    public static String dateFormatter(long date, String formatType) {
        Date convertedDate = new Date(date);

        SimpleDateFormat format = new SimpleDateFormat(formatType, locale);

        return format.format(convertedDate);
    }

    /**
     * Convert date ke format jam
     *
     * @param date tanggal yang akan dikonversi
     * @return jam setelah dikonversi (i.e. 14:10)
     */
    public static String timeFormatter(String date) {
        Date convertedDate = new Date(convertStringToLong(date));

        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault());

        return format.format(convertedDate);
    }

    /**
     * Get current date time
     *
     * @return date time format (i.e. 2015-06-04 14:20:22)
     */
    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Get current date
     *
     * @return current date (i.e. 2015-06-05)
     */
    public static String getDateNow() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Get current time
     *
     * @return time format (i.e. 14:20)
     */
    public static String getTimeNow() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Find the difference of day between two date
     *
     * @param date1    first date
     * @param date2    second date
     * @param timeUnit time unit, should TimeUnit.DAYS
     * @return number of differece (i.e 12)
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    //======================================================================
    // Notification Helper
    // 1. showNotification (Membuat notifikasi)
    //======================================================================

    /**
     * Show notification with color defined
     *
     * @param context      Intent source
     * @param resultIntent Where will you go after user click the notification
     * @param title        Notification title
     * @param text         Text that you want to inform
     * @param smallIcon    Image for small icon
     * @param color        Color background for small Icon
     */
    public static void showNotification(Context context, Intent resultIntent, String title, String text, int smallIcon, int color) {
        long[] vibrate = {1000, 2000};

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(smallIcon)
                        .setContentTitle(title)
                        .setVibrate(vibrate)
                        .setColor(color)
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentText(text);

        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        int mNotificationId = 1;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    /**
     * Show notification with default color
     *
     * @param context      Intent source
     * @param resultIntent Where will you go after user click the notification
     * @param title        Notification title
     * @param text         Text that you want to inform
     * @param smallIcon    Image for small icon
     */
    public static void showNotification(Context context, Intent resultIntent, String title, String text, int smallIcon) {
        long[] vibrate = {0, 500, 700};

        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(smallIcon)
                        .setContentTitle(title)
                        .setVibrate(vibrate)
                        .setAutoCancel(true)
                        .setSound(uri)
                        .setContentText(text);

        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        int mNotificationId = 1;
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    //======================================================================
    // Validation Helper
    //======================================================================

    /**
     * Validasi input nama, tidak boleh kosong dan minimal 3 karakter
     *
     * @param nama input
     * @return true jika sesuai dengan kondisi
     */
    public boolean validateName(EditText editText, String nama) {
        if (TextUtils.isEmpty(nama)) {
            setError(editText, "Mohon isi nama Anda!");
            return false;
        } else if (nama.length() < 3) {
            setError(editText, "Nama minimal 3 karakter.");
            return false;
        }

        return true;
    }

    /**
     * Validasi email, tidak boleh kosong dan harus email yang valid
     *
     * @param email input
     * @return true jika sesuai aturan
     */
    public boolean validateEmail(EditText editText, String email) {
        if (TextUtils.isEmpty(email)) {
            setError(editText, "Mohon isi email Anda");
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            setError(editText, "Mohon isi email Anda yang valid");
            return false;
        }
        return true;
    }

    /**
     * Validasi password, tidak boleh kosong dan minimal 6 karakter
     *
     * @param password input
     * @return true, jika sesuai aturan
     */
    public boolean validatePassword(EditText editText, String password) {
        if (TextUtils.isEmpty(password)) {
            setError(editText, "Mohon isi password Anda!");
            return false;
        } else if (password.length() < 6) {
            setError(editText, "Password minimal 6 karakter.");
            return false;
        }
        return true;
    }

    /**
     * Validasi password, tidak boleh kosong dan harus sama dengan password sebelumnya
     *
     * @param confirmPassword input
     * @return true, jika sesuai aturan
     */
    public boolean validateConfirmPassword(EditText editText, String confirmPassword, String password) {
        if (TextUtils.isEmpty(confirmPassword)) {
            setError(editText, "Mohon konfirmasi password Anda!");
            return false;
        } else if (!confirmPassword.equals(password)) {
            setError(editText, "Password harus sama.");
            return false;
        }
        return true;
    }

    /**
     * Validasi no hp, tidak boleh kosong
     *
     * @param nohp input
     * @return true jika tidak kosong
     */
    public boolean validateNohp(EditText editText, String nohp) {
        if (TextUtils.isEmpty(nohp)) {
            setError(editText, "Mohon isi No Telepon Anda!");
            return false;
        }
        return true;
    }

    public boolean isEmpty(EditText editText, String anything) {
        if (TextUtils.isEmpty(anything)) {
            setError(editText, "Tidak boleh kosong!");
            return false;
        }
        return true;
    }

    public boolean validateBirthday(EditText editText, String nohp) {
        if (TextUtils.isEmpty(nohp)) {
            setError(editText, "Mohon isi tanggal lahir Anda!");
            return false;
        }
        return true;
    }

    private void setError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
        editText.requestFocus();
    }

    /**
     * Share ke aplikasi lain
     *
     * @param context Context
     * @param text    kalimat yang ingin dishare
     */
    public static void intentShare(Context context, String subject, String text) {
        String finalText = subject + "\n\n" + text;
        Intent share = new Intent();
        share.setType("text/plain");
        share.setAction(Intent.ACTION_SEND);
        //share.putExtra(Intent.EXTRA_SUBJECT, subject);
        share.putExtra(Intent.EXTRA_TEXT, finalText);

        context.startActivity(Intent.createChooser(share, "Berbagi melalui"));
    }

    public static File compressImage(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }
}

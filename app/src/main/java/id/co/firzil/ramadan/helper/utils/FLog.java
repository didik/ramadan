package id.co.firzil.ramadan.helper.utils;

import android.util.Log;

import id.co.firzil.ramadan.BuildConfig;


/**
 * Created by didik on 10/03/16.
 *
 */
public class FLog {
    public static void d(String tag, String message){
        if(BuildConfig.DEBUG) Log.d(tag, message);
    }
}

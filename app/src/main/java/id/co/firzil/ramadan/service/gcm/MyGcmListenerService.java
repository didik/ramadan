/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.co.firzil.ramadan.service.gcm;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.feature.main.MainActivity;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.helper.utils.FUtils;


public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "GcmListenerService";

    public void onMessageReceived(String from, Bundle data) {
        FLog.d(TAG, data.toString());
        String message = data.getString("message");
        String gcm_code = data.getString("gcm_code");

        Intent i = new Intent(this, MainActivity.class);
        if (Me.getInstance().isLoggedIn() && gcm_code != null) {
            if (gcm_code.equalsIgnoreCase("new_artikel")) {
                showNotif(i, message);
            } else {
                showNotif(i, message);
            }
        }

    }

    private void showNotif(Intent i, String message) {
        FUtils.showNotification(
                this,
                i,
                getString(R.string.app_name),
                message,
                R.drawable.ic_new_article,
                Color.parseColor("#2196F3"));
    }
}

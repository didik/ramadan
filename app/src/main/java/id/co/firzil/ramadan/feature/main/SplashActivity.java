package id.co.firzil.ramadan.feature.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.app.App;
import id.co.firzil.ramadan.app.Endpoint;
import id.co.firzil.ramadan.feature.auth.LoginActivity;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.helper.utils.FUtils;
import id.co.firzil.ramadan.helper.utils.SessionChecker;
import id.co.firzil.ramadan.model.Article;
import id.co.firzil.ramadan.model.Banner;
import id.co.firzil.ramadan.model.Category;
import id.co.firzil.ramadan.model.Donation;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class SplashActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private static final int REQUEST_CODE = 123;
    private Context mContext;
    private Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        boolean isOnline = FUtils.isNetworkConnected(this);

        mContext = this;
        SessionChecker svn = new SessionChecker(this);
        svn.setIsSudahNgeCekSession(false);   //saat membuka halaman awal diset belum ngecek ke server
        svn.setLastTimeCheck(0);
        if (isOnline) {
            svn.setIsVersiTerbaru(true);
            svn.setMustUpdate(false);
            svn.setSessionStatus(SessionChecker.SESSION_STATUS_ON);
        }
        getCategory();
        getBanner();
        getArticle();
        getDonation();
        requestPermission();
    }

    private void delay() {
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    // Beri waktu 3 detik
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent i;
                    if (Me.getInstance().isLoggedIn()) i = new Intent(mContext, MainActivity.class);
                    else i = new Intent(mContext, LoginActivity.class);

                    startActivity(i);
                    finish();
                }
            }
        };
        timerThread.start();
    }

    @AfterPermissionGranted(REQUEST_CODE)
    private void requestPermission() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS)) {
            // Have permission, do the thing!
            //Toast.makeText(this, "TODO: Camera things", Toast.LENGTH_LONG).show();
            FLog.d("permission", "Permission granted! from annotaion.");
            delay();
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale),
                    REQUEST_CODE, android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        FLog.d("permission", "Permission granted!");
        //delay();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    private void getCategory() {
        Request request = new Request.Builder()
                .url(Endpoint.CATEGORY)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                if (response.isSuccessful()) {
                    try {
                        System.out.println("response: " + body);
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        Category.reset();
                        ArrayList<Category> categories = new ArrayList<Category>();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Category category = gson.fromJson(obj.toString(), Category.class);
                            categories.add(category);
                        }
                        Category.add(categories);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void getArticle() {
        Request request = new Request.Builder()
                .url(Endpoint.ARTICLE_ALL)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                if (response.isSuccessful()) {
                    try {
                        System.out.println("response: " + body);
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        Article.reset();
                        ArrayList<Article> articles = new ArrayList<>();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            articles.add(new Article(
                                    obj.getInt("id"),
                                    obj.getInt("category_id"),
                                    obj.getString("title"),
                                    obj.getString("content"),
                                    obj.getString("published"),
                                    obj.getString("image"),
                                    obj.getString("sound"),
                                    obj.getString("video")

                            ));
                        }
                        Article.add(articles);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void getDonation() {
        Request request = new Request.Builder()
                .url(Endpoint.DONATION)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                if (response.isSuccessful()) {
                    try {
                        System.out.println("donasi: " + body);
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        Donation.reset();
                        ArrayList<Donation> donations = new ArrayList<>();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            donations.add(new Donation(
                                    obj.getInt("id"),
                                    obj.getString("title"),
                                    obj.getString("content"),
                                    obj.getString("published"),
                                    obj.getString("image")

                            ));
                        }
                        Donation.add(donations);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void getBanner() {
        Request request = new Request.Builder()
                .url(Endpoint.BANNER)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                if (response.isSuccessful()) {
                    try {
                        System.out.println("response: " + body);
                        JSONObject res = new JSONObject(body);
                        JSONArray data = res.getJSONArray("data");
                        Banner.reset();
                        ArrayList<Banner> banners = new ArrayList<>();
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            Banner banner = gson.fromJson(obj.toString(), Banner.class);
                            banners.add(banner);
                        }
                        Banner.add(banners);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}

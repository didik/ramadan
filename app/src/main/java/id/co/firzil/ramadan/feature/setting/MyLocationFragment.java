package id.co.firzil.ramadan.feature.setting;


import android.app.ProgressDialog;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.helper.Geocoding;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.model.Place;
import id.co.firzil.ramadan.service.scheduler.NotificationEventReceiver;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyLocationFragment extends BaseFragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private ArrayList<Place> mPlaces;
    private double mLat = -6.1701647, mLng = 106.8292013;
    private Me me;

    public MyLocationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        me = Me.getInstance();
        String curLat = me.getData(Me.CURR_LAT);
        String realLat = me.getData(Me.LAT);

        if (!curLat.isEmpty()) {
            mLat = Double.parseDouble(curLat);
            mLng = Double.parseDouble(me.getData(Me.CURR_LNG));
        } else if (!realLat.isEmpty()) {
            mLat = Double.parseDouble(realLat);
            mLng = Double.parseDouble(me.getData(Me.LNG));
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_location, container, false);
        unbinder = ButterKnife.bind(this, view);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(true);
    }

    @OnClick(R.id.btn_set_location)
    public void onClick() {
        LatLng mLatLng = mMap.getCameraPosition().target;
        me.setData(Me.CURR_LAT, String.valueOf(mLatLng.latitude));
        me.setData(Me.CURR_LNG, String.valueOf(mLatLng.longitude));
        me.setData(Me.LAT, String.valueOf(mLatLng.latitude));
        me.setData(Me.LNG, String.valueOf(mLatLng.longitude));

        final ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Mendapatkan alamat...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Geocoding.getAddress(mLatLng, new Geocoding.Callbacks() {
            @Override
            public void onSuccess(final Address address) {
                ((AppCompatActivity) getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        FLog.d("isilocation", address.getAddressLine(0));
                        me.setData(Me.LOCATION, address.getAddressLine(0));
                        NotificationEventReceiver.setupAlarm(mContext);
                        getActivity().onBackPressed();
                        toast("Lokasi berhasil diperbarui.");
                    }
                });
            }

            @Override
            public void onError() {
                ((AppCompatActivity) getActivity()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        toast("Gagal mendapatkan alamat. Silakan coba lagi!");
                    }
                });
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(mLat,mLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
       /* LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))*/;

    }
}

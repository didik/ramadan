package id.co.firzil.ramadan.service.scheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.service.LocationService;


/**
 * Created by klogi
 * <p/>
 * Broadcast receiver for: BOOT_COMPLETED, TIMEZONE_CHANGED, and TIME_SET events. Sets Alarm Manager for notification;
 */
public final class NotificationServiceStarterReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Me.getInstance().isLoggedIn()) {
            NotificationEventReceiver.setupAlarm(context);
            context.startService(new Intent(context, LocationService.class));
        }
    }
}
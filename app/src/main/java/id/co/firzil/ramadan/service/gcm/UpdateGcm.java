package id.co.firzil.ramadan.service.gcm;

import android.content.Context;

import java.io.IOException;

import id.co.firzil.ramadan.app.App;
import id.co.firzil.ramadan.app.Const;
import id.co.firzil.ramadan.app.Endpoint;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.FLog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by didik on 23/03/16.
 * Update Gcm
 */
public class UpdateGcm {
    public void update(final Context mContext) {
        Me me = new Me(mContext);

        RequestBody requestBody = new FormBody.Builder()
                .add("gcm_id", me.getData(Me.GCM))
                .build();

        Request request = new Request.Builder()
                .url(Endpoint.SET_GCM)
                .header(Const.XAUTH, "Bearer " +me.getData(Me.API_TOKEN))
                .post(requestBody)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                FLog.d("hasilupdate", body);
                if (response.isSuccessful())
                    new GcmPreference(mContext).setIsRegisteredInServer(true);
            }
        });

    }

    public void clear(String token) {

        RequestBody requestBody = new FormBody.Builder()
                .add("gcm_id", "-")
                .build();

        Request request = new Request.Builder()
                .url(Endpoint.SET_GCM)
                .header(Const.XAUTH, "Bearer " + token)
                .post(requestBody)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String body = response.body().string();
                FLog.d("hasilupdate", body);
            }
        });

    }
}

package id.co.firzil.ramadan.feature.donasi;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.model.Donation;

/**
 * Created by didik on 21/05/16.
 * Donation
 */
public class DonasiAdapter extends RecyclerView.Adapter<DonasiAdapter.MyViewHolder> {

    private List<Donation> list;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, created;
        public ImageView pic;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_title);
            created = (TextView) view.findViewById(R.id.tv_created);
            pic = (ImageView) view.findViewById(R.id.img_picture);
        }
    }

    public DonasiAdapter(Context mContext, List<Donation> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.donasi_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Donation donation = list.get(position);

        String image = donation.getImage();
        if (image.isEmpty()) Picasso.with(mContext).load(R.drawable.sample_photo).into(holder.pic);
        else Picasso.with(mContext).load(image).into(holder.pic);
        holder.title.setText(donation.getTitle());
        holder.created.setText(donation.getCreated());

        // get drawable
        mContext.getResources().getDrawable(R.drawable.bigbang);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
package id.co.firzil.ramadan.feature.setting;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.feature.auth.LoginActivity;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.service.LocationService;
import id.co.firzil.ramadan.service.gcm.GcmPreference;
import id.co.firzil.ramadan.service.gcm.UpdateGcm;
import id.co.firzil.ramadan.service.scheduler.NotificationEventReceiver;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseFragment {

    @BindView(R.id.sc_prayer) SwitchCompat scPrayer;
    @BindView(R.id.sc_new_post) SwitchCompat scNewPost;
    @BindView(R.id.tv_address) TextView tvAddress;
    @BindView(R.id.radio_isna) RadioButton radioIsna;
    @BindView(R.id.radio_depag) RadioButton radioDepag;
    @BindView(R.id.radio_mwl) RadioButton radioMwl;

    private Me me = Me.getInstance();

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(false);

        initView();
    }

    private void initView() {
        boolean prayer = me.getBooleanData(Me.NOTIF_SHOLAT);
        boolean post = me.getBooleanData(Me.NOtiF_BERITA);

        scPrayer.setChecked(prayer);
        scNewPost.setChecked(post);

        scPrayer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                me.setData(Me.NOTIF_SHOLAT, true);
            }
        });

        scNewPost.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                me.setData(Me.NOtiF_BERITA, true);
            }
        });

        String method = me.getPrayMethod();
        if (method.equalsIgnoreCase(Me.MWL)) radioMwl.setChecked(true);
        else if (method.equalsIgnoreCase(Me.ISNA)) radioIsna.setChecked(true);
        else radioDepag.setChecked(true);

        tvAddress.setText(me.getData(Me.LOCATION));
    }

    @OnClick({R.id.view_pick_location, R.id.tv_logout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.view_pick_location:
                startFragment(new MyLocationFragment());
                break;
            case R.id.tv_logout:
                logout();
                break;
        }
    }

    @OnClick({R.id.radio_depag, R.id.radio_isna, R.id.radio_mwl})
    public void onMethodeChecked(View view) {
        boolean isChecked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.radio_depag:
                if (isChecked) me.setPrayMethod(Me.EGYPT);
                break;
            case R.id.radio_isna:
                if (isChecked) me.setPrayMethod(Me.ISNA);
                break;
            case R.id.radio_mwl:
                if (isChecked) me.setPrayMethod(Me.MWL);
                break;
        }
    }

    private void logout() {
        getActivity().stopService(new Intent(mContext, LocationService.class));
        NotificationEventReceiver.cancelAlarm(mContext);
        Me me = Me.getInstance();
        new UpdateGcm().clear(me.getData(Me.API_TOKEN));
        me.clearData();
        new GcmPreference(mContext).clear();
        startActivity(new Intent(mContext, LoginActivity.class));
        getActivity().finish();
    }
}

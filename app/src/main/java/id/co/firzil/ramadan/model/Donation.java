package id.co.firzil.ramadan.model;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by didik on 21/05/16.
 * Donation
 */
@Table(name = "Donation")
public class Donation extends Model implements Serializable {
    @Column(name = "Uid")
    private int uid;

    @Column(name = "Title")
    private String title;

    @Column(name = "Content")
    private String content;

    @Column(name = "Created")
    private String created;

    @Column(name = "Image")
    private String image;

    public Donation() {
        super();
    }

    public Donation(String title, String content, String image, String created) {
        super();
        this.title = title;
        this.content = content;
        this.created = created;
        this.image = image;
    }

    public Donation(int uid, String title, String content, String created, String image) {
        super();
        this.uid = uid;
        this.title = title;
        this.content = content;
        this.created = created;
        this.image = image;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static ArrayList<Donation> get() {
        List<Donation> list = new Select()
                .from(Donation.class)
                .execute();
        return (ArrayList<Donation>) list;
    }

    public static void add(ArrayList<Donation> donations) {
        ActiveAndroid.beginTransaction();
        try {
            for (Donation donation : donations) donation.save();
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static int size() {
        return get().size();
    }

    public static void reset() {
        new Delete().from(Donation.class).execute();
    }
}

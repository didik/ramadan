package id.co.firzil.ramadan.feature.article;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.base.RecyclerTouchListener;
import id.co.firzil.ramadan.base.SpacesItemDecoration;
import id.co.firzil.ramadan.model.Article;

/**
 * Created by didik on 20/05/16.
 * fragmet
 */
public class ArticleFragment extends BaseFragment {
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.null_content) TextView nullContent;

    public ArticleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(true);

        initRecycler();
    }

    private void initRecycler() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            int id = bundle.getInt("data");
            final ArrayList<Article> list = Article.get(id);
            if (list.size() > 0) {
                ArticleAdapter mAdapter = new ArticleAdapter(mContext, list);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
                recyclerView.setLayoutManager(mLayoutManager);
                SpacesItemDecoration itemDecoration = new SpacesItemDecoration(mContext, R.dimen.item_offset_5);
                recyclerView.addItemDecoration(itemDecoration);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);

                recyclerView.addOnItemTouchListener(new RecyclerTouchListener(mContext, recyclerView, new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        //toast(list.get(position).getTitle());
                        Article article = list.get(position);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("data", article);

                        Fragment fragment;

                        if (!article.getAudio().isEmpty()) fragment = new AudioFragment();
                        else if (!article.getVideo().isEmpty()) fragment = new VideoFragment();
                        else fragment = new ArticleDetailFragment();

                        fragment.setArguments(bundle);
                        startFragment(fragment);

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
            } else {
                recyclerView.setVisibility(View.GONE);
                nullContent.setVisibility(View.VISIBLE);
            }
        }
    }

    private ArrayList<Article> generateFound() {
        ArrayList<Article> list = new ArrayList<>();
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));
        list.add(new Article("Donation Buka Puasa Panti Al-Amin", getString(R.string.lorem), "", "2016-05-21 16:12"));

        return list;
    }
}

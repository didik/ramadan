package id.co.firzil.ramadan.feature.sahur;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.base.RecyclerTouchListener;
import id.co.firzil.ramadan.base.SpacesItemDecoration;
import id.co.firzil.ramadan.helper.utils.FUtils;
import id.co.firzil.ramadan.model.Friend;

/**
 * A simple {@link Fragment} subclass.
 */
public class SahurFragment extends BaseFragment {

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    public SahurFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sahur, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(false);

        //initRecycler();
    }

    private void initRecycler() {

        //final ArrayList<Friend> friends = generateFound();
        final ArrayList<Friend> friends = FUtils.getNumber(getActivity().getContentResolver());
        FriendAdapter mAdapter = new FriendAdapter(mContext, friends);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 3);
        recyclerView.setLayoutManager(mLayoutManager);
        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(mContext, R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(mContext, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                toast(friends.get(position).getName());
                //startFragment(new ArticleFragment());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private ArrayList<Friend> generateFound() {
        ArrayList<Friend> friends = new ArrayList<>();
        friends.add(new Friend("Daniel Redcliffe", R.drawable.daniel));
        friends.add(new Friend("Emma Watson", R.drawable.emma));
        friends.add(new Friend("Rupert Green", R.drawable.rupert));
        friends.add(new Friend("Tom Felton", R.drawable.tom));
        friends.add(new Friend("Bonnie Wright", R.drawable.bonnie));
        friends.add(new Friend("Matthew Lewis", R.drawable.matthew));
        friends.add(new Friend("Evanna Lynch", R.drawable.evy));
        friends.add(new Friend("Daniel Redcliffe", R.drawable.daniel));
        friends.add(new Friend("Emma Watson", R.drawable.emma));
        friends.add(new Friend("Rupert Green", R.drawable.rupert));
        friends.add(new Friend("Tom Felton", R.drawable.tom));
        friends.add(new Friend("Bonnie Wright", R.drawable.bonnie));
        friends.add(new Friend("Matthew Lewis", R.drawable.matthew));
        friends.add(new Friend("Evanna Lynch", R.drawable.evy));

        return friends;
    }
}

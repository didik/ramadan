package id.co.firzil.ramadan.helper.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import java.util.Calendar;

public class SessionChecker {

	private static final String
			SESSION_MESSAGE = "session_message",
			SESSION_STATUS = "session_status",
			IS_CHECKED_SESSION = "is_checked_session",
			LAST_TIME_CHECK = "last_time_check",
			END_POINT_API = "end_point_api";

	private Context context;
    public static final String SESSION_STATUS_ON = "on";

	public void clear(){
		Editor e = getSharedPreferences().edit();
		e.clear();
		e.commit();
	}

	public SessionChecker(Context c){
		context = c;
	}
	
	private SharedPreferences getSharedPreferences() {
		return context.getSharedPreferences("session_checker_prefs", Context.MODE_PRIVATE);
	}
	
	private void commit(String key, String value) {
		Editor editor = getSharedPreferences().edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	private String getValue(String key) {
		return getSharedPreferences().getString(key, "");
	}

	public void setSessionStatus(String sessionStatus) {
		commit(SESSION_STATUS, sessionStatus);
	}

	public boolean isSessionStatusOn(){
		return getSharedPreferences().getString(SESSION_STATUS, SESSION_STATUS_ON).equalsIgnoreCase(SESSION_STATUS_ON);
	}

	public void setIsSudahNgeCekSession(boolean sudah) {
		commit(IS_CHECKED_SESSION, sudah ? "1" : "");
	}

	public boolean isBelumNgecekSession(){
		return TextUtils.isEmpty(getValue(IS_CHECKED_SESSION));
	}

	public void setSessionMessage(String sessionMessage) {
		commit(SESSION_MESSAGE, sessionMessage);
	}

	public String getSessionMessage(){
		return getValue(SESSION_MESSAGE);
	}

	public void setLastTimeCheck(long lastTimeCheck) {
		commit(LAST_TIME_CHECK, lastTimeCheck+"");
	}

	public long getLastTimeCheck(){
		String waktu = getValue(LAST_TIME_CHECK);
		if(TextUtils.isEmpty(waktu)) return 0;
		else return Long.parseLong(waktu);
	}

	public boolean isSudah30menit(){
		final int SECOND_MILLIS = 1000;
		final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
		long time = getLastTimeCheck();
		long now = Calendar.getInstance().getTimeInMillis();

		final long diff = now - time;
		boolean sudah_30_menit = true;
		if(diff < 30 * MINUTE_MILLIS) sudah_30_menit = false;

		//Log.d("waktu", "waktu = "+sudah_30_menit+" last = "+time+" now = "+now);

		return sudah_30_menit;
	}

	private static final String
			MUST_UPDATE = "must_update",
			IS_CHECKED_VERSION = "is_checked_version",
			URL_UPDATE = "url_update",
			IS_UPDATED_VERSION = "is_updated_version";
	public static final String PLAYSTORE = "playstore";

	public void setMustUpdate(boolean must_update) {
		commit(MUST_UPDATE, must_update ? "1" : "");
	}

	public boolean isMustUpdate(){
		return ! TextUtils.isEmpty(getValue(MUST_UPDATE));
	}

	public void setIsSudahNgeCekVersi(boolean sudah) {
		commit(IS_CHECKED_VERSION, sudah ? "1" : "");
	}

	public boolean isBelumNgecekVersi(){
		return TextUtils.isEmpty(getValue(IS_CHECKED_VERSION));
	}

	public void setUrlUpdate(String v) {
		commit(URL_UPDATE, v);
	}

	public String getUrlUpdate(){
		return getValue(URL_UPDATE);
	}

	public void setIsVersiTerbaru(boolean versi_terbaru){
		commit(IS_UPDATED_VERSION, versi_terbaru ? "1" : "");
	}

	public boolean isVersiTerbaru(){
		return ! TextUtils.isEmpty(getValue(IS_UPDATED_VERSION));
	}

	public void setEndPointApi(String end_point_api){
		commit(END_POINT_API, end_point_api);
	}

	public String getEndPointApi(){
		return getValue(END_POINT_API);
	}

}
package id.co.firzil.ramadan.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.jetradar.multibackstack.BackStackActivity;

import butterknife.Unbinder;
import id.co.firzil.ramadan.BuildConfig;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.helper.handler.ExceptionHandler;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.Checker;
import id.co.firzil.ramadan.service.gcm.GcmPreference;
import id.co.firzil.ramadan.service.gcm.UpdateGcm;

/**
 * Created by didik on 27/05/16.
 * Special
 */
public abstract class SpecialActivity extends BackStackActivity {
    protected Context mContext;
    private Checker checker;
    protected Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BuildConfig.DEBUG)
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        mContext = this;
    }

    protected void onResume() {
        super.onResume();

        if (checker == null) {
            checker = new Checker(this);
            checker.setOnCloseClickListener(new Checker.OnCloseClickListener() {   //set listener ketika tombol close dialog di klik
                @Override
                public void onClose() {
                    finish();
                }
            });
        }
        checker.cekSession();   //lalu ngecek
        //FUtils.isLocationEnabled(mContext);
        Me me = Me.getInstance();

        //jika gcm token gk kosong dan gcm token belum di kirim ke server
        if ((me.isLoggedIn()) && (!TextUtils.isEmpty(me.getData(Me.GCM))) && (!new GcmPreference(this).isRegisteredInServer())) {
            new UpdateGcm().update(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (unbinder != null) unbinder.unbind();
    }

    protected void toast(String message, int length) {
        Toast.makeText(mContext, message, length).show();
    }

    protected void toast(String message) {
        toast(message, Toast.LENGTH_SHORT);
    }

   /* @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.scale_in, R.anim.slide_out_to_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.scale_in, R.anim.slide_out_to_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.scale_out);
    }
}

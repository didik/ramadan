package id.co.firzil.ramadan.model;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by didik on 20/05/16.
 * Category
 */
@Table(name = "Category")
public class Category extends Model implements Serializable {

    @SerializedName("id")
    @Expose
    @Column(name = "Uid")
    private Integer uid;

    @SerializedName("name")
    @Expose
    @Column(name = "Name")
    private String name;

    @SerializedName("image")
    @Expose
    @Column(name = "Image")
    private String image;

    public Category() {
        super();
    }

    public Category(String name, String image) {
        this.name = name;
        this.image = image;
    }

    /**
     * @return The id
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * @param id The id
     */
    public void setUid(Integer id) {
        this.uid = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    public static ArrayList<Category> get() {
        List<Category> histories = new Select()
                .from(Category.class)
                .execute();
        return (ArrayList<Category>) histories;
    }

    public static void add(ArrayList<Category> categories) {
        ActiveAndroid.beginTransaction();
        try {
            for (Category category : categories) category.save();
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static int size(){
        return get().size();
    }

    public static void reset() {
        new Delete().from(Category.class).execute();
    }

}

package id.co.firzil.ramadan.helper.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONObject;

import java.net.URL;
import java.util.Date;

import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.app.App;
import okhttp3.Request;
import okhttp3.Response;


public class Checker {
    private Context c;
    private MaterialDialog dialog_session, dialog_version;
    private SessionChecker svc;
    private OnCloseClickListener onCloseClick;

    public interface OnCloseClickListener{
        void onClose();
    }

    public Checker(Context c){
        this.c = c;
        svc = new SessionChecker(c);
    }

    public void setOnCloseClickListener(OnCloseClickListener onCloseClick){
        this.onCloseClick = onCloseClick;
    }

    private void showDialogOffApp(){
        try{
            if(dialog_session == null){
                dialog_session = new MaterialDialog.Builder(c)
                        .positiveText("Close")
                        .positiveColorRes(R.color.colorPrimary)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                                if (onCloseClick != null) onCloseClick.onClose();
                            }
                        })
                        .title("Session Checker")
                        .content(svc.getSessionMessage())
                        .cancelable(false)
                        .build();

                dialog_session.setCanceledOnTouchOutside(false);
            }

            if(! dialog_session.isShowing()) dialog_session.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void cekSession(){
        try {
            if(isOnLine() && (svc.isBelumNgecekSession() || svc.isSudah30menit())){
                new AsyncTask<String, String, String>(){
                    JSONObject j;

                    @Override
                    protected String doInBackground(String... params) {
                        try{
                            URL url = new URL("http://versioncheck.mlogg.com/index.php/api/version/check/"+c.getPackageName()+"/?mobile_version="+getAppVersion(c));
                            /*HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setReadTimeout(15000);
                            conn.setConnectTimeout(15000);
                            conn.setRequestMethod("GET");
                            conn.connect();

                            InputStream in = conn.getInputStream();

                            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf-8"), 8);
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while ((line = reader.readLine()) != null) {
                                sb.append(line).append("\n");
                            }
                            in.close();

                            String json = sb.toString();*/

                            Request request = new Request.Builder()
                                    .url(url)
                                    .build();

                            Response response = App.getClient().newCall(request).execute();

                            String json = response.body().string();

                            FLog.d("CHECKER RESPONSE", "CHECKER RESPONSE " + json);

                            j = new JSONObject(json);
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }
                        return null;
                    }

                    protected void onPostExecute(String r){
                        super.onPostExecute(r);
                        if (j != null) {
                            try {
                                if (j.optInt("status_code") == 1) {
                                    svc.setLastTimeCheck(new Date().getTime());
                                    svc.setSessionStatus(j.optString("session_status"));
                                    svc.setSessionMessage(j.optString("session_message"));
                                    svc.setIsSudahNgeCekSession(true);

                                    boolean harusUpdate = j.optBoolean("status_mandatory");
                                    String endpoint = j.optString("end_point");

                                    JSONObject status_app = j.getJSONObject("status_app");
                                    boolean is_terbaru = status_app.optBoolean("is_terbaru");
                                    String url_update = status_app.optString("url_update");

                                    svc.setIsVersiTerbaru(is_terbaru);
                                    svc.setMustUpdate(harusUpdate);
                                    svc.setUrlUpdate(url_update);
                                    svc.setIsSudahNgeCekVersi(true);
                                    svc.setEndPointApi(endpoint);

                                    if (! svc.isSessionStatusOn()) showDialogOffApp();
                                    else if (!svc.isVersiTerbaru()) showDialogUpdate();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
            else if(! svc.isSessionStatusOn()) showDialogOffApp();
            else if(! svc.isVersiTerbaru()) showDialogUpdate();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showDialogUpdate(){
        try{
            if(dialog_version == null){
                dialog_version = new MaterialDialog.Builder(c)
                        .positiveText("Update")
                        .positiveColorRes(R.color.colorPrimary)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                                String url = svc.getUrlUpdate();
                                if (url.equalsIgnoreCase(SessionChecker.PLAYSTORE)) {
                                    String appPackageName = c.getPackageName();
                                    try {
                                        c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        dismissDialog();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(c, "No Play Store Application Installed", Toast.LENGTH_LONG).show();
                                        loadUrlApk("https://play.google.com/store/apps/details?id=" + appPackageName);
                                    }
                                } else loadUrlApk(url);
                            }
                        })
                        .title("Version Checker")
                        .content("Update version has been released. Update now!")
                        .build();

                dialog_version.setCanceledOnTouchOutside(false);

            }
            dialog_version.setCancelable(! svc.isMustUpdate());
            if(! dialog_version.isShowing()) dialog_version.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadUrlApk(String url_apk){
        try {
            c.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url_apk)));
            dismissDialog();
        }
        catch (android.content.ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            Toast.makeText(c, anfe.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void dismissDialog(){
        if(dialog_session != null && dialog_session.isShowing()) dialog_session.dismiss();
        if(dialog_version != null && dialog_version.isShowing()) dialog_version.dismiss();
    }

    private boolean isOnLine(){
        try{
            ConnectivityManager connectivity = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null){
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null){
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return false;
    }
    private String getAppVersion(Context c){
        String versi = "";
        PackageManager manager = c.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(c.getPackageName(), 0);
            versi = info.versionName;
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versi;
    }


}
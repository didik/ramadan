package id.co.firzil.ramadan.feature.article;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.helper.utils.FUtils;
import id.co.firzil.ramadan.model.Article;

/**
 * Created by didik on 21/05/16.
 * Detail article
 */
public class ArticleDetailFragment extends BaseFragment {
    @BindView(R.id.img_banner) ImageView imgBanner;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_date) TextView tvDate;
    @BindView(R.id.tv_content) TextView tvContent;

    private String title, content;

    public ArticleDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_article_detail, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(true);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Article article = (Article) bundle.getSerializable("data");
            String foto = article.getImage();
            if (!foto.isEmpty()) Picasso.with(mContext).load(foto).into(imgBanner);
            else Picasso.with(mContext).load(R.drawable.sample_photo).into(imgBanner);
            title = article.getTitle();
            content = String.valueOf(Html.fromHtml(article.getContent()));
            tvTitle.setText(title);
            tvDate.setText(article.getCreated());
            tvContent.setText(content);
            tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.ic_action_share) {
            FUtils.intentShare(mContext, title, content);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

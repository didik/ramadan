package id.co.firzil.ramadan.app;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

import id.co.firzil.ramadan.helper.manager.Me;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

/**
 * Created by didik on 22/04/16.
 * App
 */
public class App extends Application {
    private static OkHttpClient client;

    @Override
    public void onCreate() {
        super.onCreate();

        Me.init(this);

        // Inisialisasi Active Android
        ActiveAndroid.initialize(this);

        // Set cache untuk OkHttp
        int cacheSize = 100 * 1024 * 1024; // 100 MiB
        Cache cache = new Cache(getApplicationContext().getCacheDir(), cacheSize);
        client = new OkHttpClient.Builder()
                .cache(cache)
                .build();
    }

    public static OkHttpClient getClient() {
        return client;
    }
}

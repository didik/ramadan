package id.co.firzil.ramadan.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Unbinder;
import id.co.firzil.ramadan.feature.main.MainActivity;
import id.co.firzil.ramadan.model.MyPair;

/**
 * Created by didik on 22/04/16.
 * BaseFragment
 */
public abstract class BaseFragment extends Fragment {
    protected Context mContext;
    protected Unbinder unbinder;
    protected ActionBar mActionBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActionBar = ((MainActivity) getActivity()).getSupportActionBar();
        setHasOptionsMenu(true);
    }

    protected void toast(String message, int length) {
        Toast.makeText(mContext, message, length).show();
    }

    protected void toast(String message) {
        toast(message, Toast.LENGTH_SHORT);
    }

   /* @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_from_right, R.anim.scale_out);
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) unbinder.unbind();
    }

    protected void startFragment(Fragment fragment) {
        ((MainActivity) getActivity()).showFragment(fragment);
    }

    protected void startFragment(Fragment fragment, ArrayList<MyPair> pairs) {
        ((MainActivity) getActivity()).showFragment(fragment, pairs);
    }

    @SuppressWarnings("ConstantConditions")
    protected void showHomeButton(boolean enable) {
        mActionBar.setDisplayHomeAsUpEnabled(enable);
    }
}

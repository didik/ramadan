package id.co.firzil.ramadan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by didik on 23/05/16.
 * User
 */
public class User {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("api_token")
    @Expose
    private String apiToken;
    @SerializedName("gcm_id")
    @Expose
    private String gcmId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("registered")
    @Expose
    private String registered;


    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The apiToken
     */
    public String getApiToken() {
        return apiToken;
    }

    /**
     * @param apiToken The api_token
     */
    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    /**
     * @return The gcmId
     */
    public String getGcmId() {
        return gcmId;
    }

    /**
     * @param gcmId The gcm_id
     */
    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", apiToken='" + apiToken + '\'' +
                ", gcmId='" + gcmId + '\'' +
                ", phone='" + phone + '\'' +
                ", registered='" + registered + '\'' +
                '}';
    }
}

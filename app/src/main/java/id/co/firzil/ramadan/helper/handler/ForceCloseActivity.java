package id.co.firzil.ramadan.helper.handler;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class ForceCloseActivity extends AppCompatActivity {
	public static final String ERROR_MSG = "error";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int pad = 12;
		TextView tv = new TextView(this);
		tv.setText("ERROR\n\n" + getIntent().getStringExtra(ERROR_MSG));
		tv.setLayoutParams(new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.MATCH_PARENT));
		tv.setPadding(pad, pad, pad, pad);
		tv.setBackgroundColor(Color.WHITE);
		tv.setTextColor(Color.BLACK);

		ScrollView s = new ScrollView(this);
		s.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		s.addView(tv);

		setContentView(s);
	}
}

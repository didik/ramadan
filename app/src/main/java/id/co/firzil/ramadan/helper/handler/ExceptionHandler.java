package id.co.firzil.ramadan.helper.handler;

import android.app.Activity;
import android.content.Intent;


public class ExceptionHandler implements
		Thread.UncaughtExceptionHandler {
	private final Activity myContext;

	public ExceptionHandler(Activity context) {
		myContext = context;
	}

	public void uncaughtException(Thread thread, Throwable exception) {
		Intent intent = new Intent(myContext, ForceCloseActivity.class);
		intent.putExtra(ForceCloseActivity.ERROR_MSG, ErrorWriter.getError(exception));
		myContext.startActivity(intent);

		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(10);
	}

}
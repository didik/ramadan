package id.co.firzil.ramadan.feature.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.app.App;
import id.co.firzil.ramadan.app.Endpoint;
import id.co.firzil.ramadan.app.Messages;
import id.co.firzil.ramadan.base.BaseActivity;
import id.co.firzil.ramadan.feature.main.MainActivity;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.FUtils;
import id.co.firzil.ramadan.model.User;
import id.co.firzil.ramadan.service.LocationService;
import id.co.firzil.ramadan.service.scheduler.NotificationEventReceiver;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_email) EditText etEmail;
    @BindView(R.id.et_pass) EditText etPass;
    @BindView(R.id.txt_error) TextView txtError;

    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        unbinder = ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_login, R.id.btn_register})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                validateForm();
                break;
            case R.id.btn_register:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    private void validateForm() {
        String email = etEmail.getText().toString();
        String password = etPass.getText().toString();

        if (FUtils.isNetworkConnected(this)) {
            FUtils v = new FUtils();
            if (!v.validateEmail(etEmail, email)) return;
            if (!v.validatePassword(etPass, password)) return;

            doLogin(email, password);
        } else toast(Messages.NO_INTERNET);
    }

    private void doLogin(String email, final String pass) {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage(Messages.LOGGING_IN);
        pDialog.setCancelable(false);
        pDialog.show();

        RequestBody requestBody = new FormBody.Builder()
                .add("email", email)
                .add("password", pass)
                .build();

        Request request = new Request.Builder()
                .url(Endpoint.LOGIN)
                .post(requestBody)
                .build();

        App.getClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismiss();
                        toast(Messages.FAILED_TO_CONNECT);
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String body = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(body);
                            if (response.isSuccessful()) {
                                User user = gson.fromJson(res.getJSONObject("data").toString(), User.class);
                                Me.getInstance().setLogin(user);
                                NotificationEventReceiver.setupAlarm(mContext);
                                startService(new Intent(mContext, LocationService.class));
                                startActivity(new Intent(mContext, MainActivity.class));
                                finish();
                            } else {
                                txtError.setVisibility(View.VISIBLE);
                                txtError.setText(body);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });
    }

}

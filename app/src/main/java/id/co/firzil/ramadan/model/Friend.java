package id.co.firzil.ramadan.model;

import java.io.Serializable;

/**
 * Created by didik on 21/05/16.
 * Friend
 */
public class Friend implements Serializable {
    private int fid;
    private String name;
    private int photo;
    private String phone;

    public Friend(String name, int photo) {
        this.name = name;
        this.photo = photo;
    }

    public Friend(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}

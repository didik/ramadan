package id.co.firzil.ramadan.feature.adzan;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.ButterKnife;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.helper.NearbyPlace;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.model.Place;

/**
 * Created by didik on 21/05/16.
 * Place
 */
public class NearbyFragment extends BaseFragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<Place> mPlaces;
    private double mLat = -6.1701647, mLng = 106.8292013;

    public NearbyFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Me me = Me.getInstance();
        String curLat = me.getData(Me.CURR_LAT);
        String realLat = me.getData(Me.LAT);

        if (!curLat.isEmpty()) {
            mLat = Double.parseDouble(curLat);
            mLng = Double.parseDouble(me.getData(Me.CURR_LNG));
        } else if (!realLat.isEmpty()) {
            mLat = Double.parseDouble(realLat);
            mLng = Double.parseDouble(me.getData(Me.LNG));
        }

        View view = inflater.inflate(R.layout.fragment_nearby, container, false);
        unbinder = ButterKnife.bind(this, view);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(true);
    }

   /* @OnClick(R.id.btn_src_mosque)
    public void onClick() {
    }*/

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(mLat, mLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
        getNearbyMosque();
    }

    private void getNearbyMosque() {
        NearbyPlace.get(mContext, mLat, mLng, new NearbyPlace.Callbacks() {
            @Override
            public void onSuccess(ArrayList<Place> places) {
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_pin);
                mPlaces = places;
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (Place place : places) {
                    LatLng latLng = new LatLng(
                            place.getLat(),
                            place.getLng());
                    mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            //.icon(icon)
                            .snippet(place.getVicinity())
                            .title(place.getName()));
                    builder.include(latLng);
                }
                LatLngBounds bounds = builder.build();

                final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
                mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        mMap.animateCamera(cu);
                        mMap.setOnCameraChangeListener(null);
                    }
                });

                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        double lat = 0, lng = 0;
                        for (Place mPlace : mPlaces) {
                            if (mPlace.getName().equalsIgnoreCase(marker.getTitle())) {
                                lat = mPlace.getLat();
                                lng = mPlace.getLng();
                            }
                        }

                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lng);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                });
            }

            @Override
            public void onError() {
                toast("Gagal mendapatkan masjid terdekat!");
            }
        });
    }
}

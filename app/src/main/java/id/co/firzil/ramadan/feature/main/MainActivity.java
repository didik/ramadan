package id.co.firzil.ramadan.feature.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.SpecialActivity;
import id.co.firzil.ramadan.feature.adzan.AdzanFragment;
import id.co.firzil.ramadan.feature.donasi.DonasiFragment;
import id.co.firzil.ramadan.feature.sahur.SahurFragment;
import id.co.firzil.ramadan.feature.setting.SettingFragment;
import id.co.firzil.ramadan.helper.utils.FUtils;
import id.co.firzil.ramadan.model.MyPair;
import id.co.firzil.ramadan.service.gcm.RegistrationIntentService;

public class MainActivity extends SpecialActivity {

    @BindView(R.id.bottom_navigation) AHBottomNavigation bottomNavigation;
    @BindView(R.id.toolbar) Toolbar toolbar;

    private ArrayList<Fragment> fragments;
    private static final String STATE_TAB_ID = "tab_id";
    private static final int MAIN_TAB_ID = 0;

    private Fragment curFragment;
    private int curTabId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        startService(new Intent(this, RegistrationIntentService.class));

        fragments = new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new AdzanFragment());
        fragments.add(new SahurFragment());
        fragments.add(new DonasiFragment());
        fragments.add(new SettingFragment());

        initBottomNav();

        FUtils.isLocationEnabled(this);
    }

    private void initBottomNav() {
        ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

        AHBottomNavigationItem itemHome = new AHBottomNavigationItem("Home", R.drawable.ic_home);
        AHBottomNavigationItem itemAdzan = new AHBottomNavigationItem("Adzan", R.drawable.ic_adzan);
        AHBottomNavigationItem itemSahur = new AHBottomNavigationItem("Sahur", R.drawable.ic_sahur);
        AHBottomNavigationItem itemDonasi = new AHBottomNavigationItem("Donation", R.drawable.ic_donasi);
        AHBottomNavigationItem itemSetting = new AHBottomNavigationItem("Setting", R.drawable.ic_setting);

        bottomNavigationItems.add(itemHome);
        bottomNavigationItems.add(itemAdzan);
        bottomNavigationItems.add(itemSahur);
        bottomNavigationItems.add(itemDonasi);
        bottomNavigationItems.add(itemSetting);

        assert bottomNavigation != null;
        bottomNavigation.addItems(bottomNavigationItems);
        bottomNavigation.setBehaviorTranslationEnabled(false);
        bottomNavigation.setAccentColor(Color.parseColor("#2196F3"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, boolean wasSelected) {
                if (curFragment != null) {
                    pushToBackStack(curTabId, curFragment);
                }
                curTabId = position;
                curFragment = popFromBackStack(curTabId);
                if (curFragment == null) {
                    curFragment = fragmentByTab(curTabId);
                }
                replaceFragment(curFragment);
            }
        });

        if (getSupportFragmentManager().findFragmentById(R.id.content) == null) {
            bottomNavigation.setCurrentItem(MAIN_TAB_ID, false);
            showFragment(fragmentByTab(MAIN_TAB_ID));
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        curFragment = getSupportFragmentManager().findFragmentById(R.id.content);
        curTabId = savedInstanceState.getInt(STATE_TAB_ID);
        bottomNavigation.setCurrentItem(curTabId, false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_TAB_ID, curTabId);
        super.onSaveInstanceState(outState);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onBackPressed() {
        if (!backStackManager.empty()) {
            Pair<Integer, Fragment> pair = popFromBackStack();
            backTo(pair.first, pair.second);
        } else {
            super.onBackPressed();
        }
    }


    private Fragment fragmentByTab(int tabId) {
        switch (tabId) {
            case 0:
                return fragments.get(0);
            case 1:
                return fragments.get(1);
            case 2:
                return fragments.get(2);
            case 3:
                return fragments.get(3);
            case 4:
                return fragments.get(4);
            default:
                throw new IllegalArgumentException();
        }
    }

    public void showFragment(@NonNull Fragment fragment) {
        showFragment(fragment, true);
    }

    public void showFragment(@NonNull Fragment fragment, boolean addToBackStack) {
        if (curFragment != null && addToBackStack) {
            pushToBackStack(curTabId, curFragment);
        }
        curFragment = fragment;
        replaceFragment(fragment);
    }

    public void showFragment(@NonNull Fragment fragment, ArrayList<MyPair> pairs) {
        showFragment(fragment, true, pairs);
    }

    public void showFragment(@NonNull Fragment fragment, boolean addToBackStack, ArrayList<MyPair> pairs) {
        if (curFragment != null && addToBackStack) {
            pushToBackStack(curTabId, curFragment);
        }
        curFragment = fragment;
        replaceFragment(fragment, pairs);
    }

    private void backTo(int tabId, @NonNull Fragment fragment) {
        curTabId = tabId;
        bottomNavigation.setCurrentItem(curTabId, false);
        curFragment = fragment;
        replaceFragment(fragment);
        getSupportFragmentManager().executePendingTransactions();
    }

    private void backToRoot() {
        Fragment originalRootFragment = fragmentByTab(curTabId);
        if (curFragment.getClass() != originalRootFragment.getClass()) {
            Fragment rootFragment = popRootFromBackStack(curTabId);
            if (rootFragment == null || rootFragment.getClass() != originalRootFragment.getClass()) {
                rootFragment = originalRootFragment;
            }
            curFragment = rootFragment;
            replaceFragment(rootFragment);
        }
    }

    private void replaceFragment(@NonNull Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.content, fragment);
        tr.commit();
    }

    private void replaceFragment(@NonNull Fragment fragment, ArrayList<MyPair> pairs) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        for (MyPair pair : pairs)
            tr.addSharedElement(pair.getView(), pair.getName());
        tr.replace(R.id.content, fragment);
        tr.commit();
    }
}

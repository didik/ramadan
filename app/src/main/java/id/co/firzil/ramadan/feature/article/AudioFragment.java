package id.co.firzil.ramadan.feature.article;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.helper.AudioPlayerView;
import id.co.firzil.ramadan.model.Article;

/**
 * Created by didik on 30/05/16.
 * Audio
 */
public class AudioFragment extends BaseFragment {
    @BindView(R.id.play) AudioPlayerView audioPlayerView;
    @BindView(R.id.frame) FrameLayout frame;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_date) TextView tvDate;
    @BindView(R.id.tv_content) TextView tvContent;
    @BindView(R.id.img_banner) ImageView imgBanner;

    public AudioFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(true);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            Article article = (Article) bundle.getSerializable("data");
            String foto = article.getImage();
            if (!foto.isEmpty()) Picasso.with(mContext).load(foto).into(imgBanner);
            else Picasso.with(mContext).load(R.drawable.sample_photo).into(imgBanner);
            tvTitle.setText(article.getTitle());
            tvDate.setText(article.getCreated());
            tvContent.setText(Html.fromHtml(article.getContent()));
            tvContent.setMovementMethod(LinkMovementMethod.getInstance());

            audioPlayerView.withUrl(article.getAudio());
            audioPlayerView.setOnAudioPlayerViewListener(new AudioPlayerView.OnAudioPlayerViewListener() {
                @Override
                public void onAudioPreparing() {
                    toast("Preparing audio..");
                }

                @Override
                public void onAudioReady() {
                    toast("Audio is ready.");
                }

                @Override
                public void onAudioFinished() {
                    toast("Audio is finished.");
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        audioPlayerView.destroy();
        super.onDestroyView();
    }
}

package id.co.firzil.ramadan.service.scheduler;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import java.util.HashMap;

import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.feature.main.MainActivity;
import id.co.firzil.ramadan.helper.Prayer;
import id.co.firzil.ramadan.helper.utils.FUtils;


/**
 * Created by klogi
 */
public class NotificationIntentService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";

    public NotificationIntentService() {
        super(NotificationIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartNotificationService(Context context) {
        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(getClass().getSimpleName(), "onHandleIntent, started handling a notification event");
        try {
            String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                processStartNotification();
            }
        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }

    }

    private void processDeleteNotification(Intent intent) {
        // Log something?
    }

    private void processStartNotification() {
        HashMap<String, String> timeNames = new HashMap<>();
        timeNames.put("Zuhr", "Fajr");
        timeNames.put("Asr", "Zuhr");
        timeNames.put("Maghrib", "Asr");
        timeNames.put("Isha", "Maghrib");
        timeNames.put("Fajr", "Isha");

        String prayName = Prayer.getNearPray().getName();
        showNotif(timeNames.get(prayName));
        NotificationEventReceiver.setupAlarm(this);
    }

    private void showNotif(String prayName) {
        FUtils.showNotification(
                this,
                new Intent(this, MainActivity.class),
                getString(R.string.app_name),
                "It's time to " + prayName + " prayer.",
                R.drawable.ic_adzan_time,
                Color.parseColor("#2196F3")
        );
    }
}

package id.co.firzil.ramadan.helper.prayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

public class TimeNames {
	private static final ArrayList<String> names = new ArrayList<>(Arrays.asList("Fajr", "Sunrise", "Zuhr", "Asr", "Maghrib", "Isha", "Imsak"));

    private static TimeNames instance;

    private ResourceBundle res;
    private Locale locale;

    public TimeNames(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    /**
     * @param type one of prayer time constants in {@link PrayerTimes}, including {@link PrayerTimes#SUNRISE}
     * @return
     */
    public String get(int type) {
        if (type >= 6 || type < 0)
            throw new IllegalArgumentException("Invalid time type: " + type);
        //return res.getString("time." + type);
        return names.get(type);
    }

    public String getImsak() {
        //return res.getString("time.imsak");
        return names.get(6);
    }

    public static TimeNames getInstance(Locale locale) {
        if (instance == null || !instance.locale.equals(locale))
            instance = new TimeNames(locale);
        return instance;
    }

}

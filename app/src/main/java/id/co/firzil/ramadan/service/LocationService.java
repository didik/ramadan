package id.co.firzil.ramadan.service;

import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.IBinder;

import com.google.android.gms.maps.model.LatLng;

import id.co.firzil.ramadan.helper.Geocoding;
import id.co.firzil.ramadan.helper.LocationProvider;
import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.service.scheduler.NotificationEventReceiver;

public class LocationService extends Service implements LocationProvider.LocationCallback {
    private LocationProvider mLocationProvider;
    private Me me;

    public LocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationProvider = new LocationProvider(this, this);
        mLocationProvider.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLocationProvider != null) mLocationProvider.disconnect();
    }

    @Override
    public void handleNewLocation(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        FLog.d("location", "lat: " + lat + ",lng: " + lng);
        me = Me.getInstance();
        me.setData(Me.LAT, String.valueOf(lat));
        me.setData(Me.LNG, String.valueOf(lng));
        convertToAddress(new LatLng(lat,lng));
        NotificationEventReceiver.setupAlarm(this);
    }

    private void convertToAddress(LatLng latLng){
        Geocoding.getAddress(latLng, new Geocoding.Callbacks() {
            @Override
            public void onSuccess(Address address) {
                me.setData(Me.LOCATION, address.getAddressLine(0));
            }

            @Override
            public void onError() {

            }
        });
    }
}

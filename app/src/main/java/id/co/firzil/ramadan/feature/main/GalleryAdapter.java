package id.co.firzil.ramadan.feature.main;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.helper.SquareLayout;
import id.co.firzil.ramadan.model.Category;

/**
 * Created by didik on 20/05/16.
 * Adapter
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private List<Category> categories;
    private Context mContext;
    private List<Integer> allColors;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public SquareLayout squareLayout;
        public TextView name;

        public MyViewHolder(View view) {
            super(view);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            squareLayout = (SquareLayout) view.findViewById(R.id.square);
            name = (TextView) view.findViewById(R.id.tv_name);
        }
    }


    public GalleryAdapter(Context context, List<Category> categories) {
        mContext = context;
        this.categories = categories;
        try {
            allColors = getAllMaterialColors();
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_thumbnail, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        int randomIndex = new Random().nextInt(allColors.size());
        int randomColor = allColors.get(randomIndex);

        Category category = categories.get(position);

        String logo = category.getImage();
        if (logo.isEmpty()) holder.thumbnail.setVisibility(View.GONE);
        else Picasso.with(mContext).load(logo).into(holder.thumbnail);
        holder.name.setText(category.getName());
        holder.squareLayout.setBackgroundColor(randomColor);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    private List<Integer> getAllMaterialColors() throws IOException, XmlPullParserException {
        XmlResourceParser xrp = mContext.getResources().getXml(R.xml.materialcolor);
        List<Integer> allColors = new ArrayList<>();
        int nextEvent;
        while ((nextEvent = xrp.next()) != XmlResourceParser.END_DOCUMENT) {
            String s = xrp.getName();
            if ("color".equals(s)) {
                String color = xrp.nextText();
                allColors.add(Color.parseColor(color));
            }
        }
        return allColors;
    }
}

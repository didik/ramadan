package id.co.firzil.ramadan.model;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by didik on 21/05/16.
 * Article
 */
@Table(name = "Article")
public class Article extends Model implements Serializable {
    @Column(name = "Uid")
    private int uid;

    @Column(name = "CategoryId")
    private int categoryid;

    @Column(name = "Title")
    private String title;

    @Column(name = "Content")
    private String content;

    @Column(name = "Created")
    private String created;

    @Column(name = "Image")
    private String image;

    @Column(name = "Audio")
    private String audio;

    @Column(name = "Video")
    private String video;

    public Article() {
        super();
    }

    public Article(String title, String content, String image, String created) {
        super();
        this.title = title;
        this.content = content;
        this.created = created;
        this.image = image;
    }

    public Article(int uid, int categoryid, String title, String content, String created, String image) {
        super();
        this.uid = uid;
        this.categoryid = categoryid;
        this.title = title;
        this.content = content;
        this.created = created;
        this.image = image;
    }

    public Article(int uid, int categoryid, String title, String content, String created, String image, String audio, String video) {
        this.uid = uid;
        this.categoryid = categoryid;
        this.title = title;
        this.content = content;
        this.created = created;
        this.image = image;
        this.audio = audio;
        this.video = video;
    }

    public int getArticleid() {
        return categoryid;
    }

    public void setArticleid(int categoryid) {
        this.categoryid = categoryid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public static ArrayList<Article> get(int id) {
        List<Article> list = new Select()
                .from(Article.class)
                .where("CategoryId = ?", id)
                .execute();
        return (ArrayList<Article>) list;
    }

    public static ArrayList<Article> get() {
        List<Article> list = new Select()
                .from(Article.class)
                .execute();
        return (ArrayList<Article>) list;
    }

    public static void add(ArrayList<Article> articles) {
        ActiveAndroid.beginTransaction();
        try {
            for (Article article : articles) article.save();
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static int size() {
        return get().size();
    }

    public static void reset() {
        new Delete().from(Article.class).execute();
    }
}

package id.co.firzil.ramadan.helper;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import id.co.firzil.ramadan.helper.manager.Me;
import id.co.firzil.ramadan.helper.prayer.Method;
import id.co.firzil.ramadan.helper.prayer.PrayerTime;
import id.co.firzil.ramadan.helper.prayer.PrayerTimeCalc;
import id.co.firzil.ramadan.helper.prayer.PrayerTimes;
import id.co.firzil.ramadan.helper.prayer.TimeNames;
import id.co.firzil.ramadan.helper.prayer.astro.Location;

/**
 * Created by didik on 30/05/16.
 * Helper
 */
public class Prayer {

    private static double timezone = (Calendar.getInstance().getTimeZone()
            .getOffset(Calendar.getInstance().getTimeInMillis()))
            / (1000 * 60 * 60);
    private static Me me = Me.getInstance();

    public static PrayerTimes getPrayerTimes(Date date) {
        LatLng latLng = getLatLng();
        Location location = new Location(latLng.latitude, latLng.longitude, timezone, 0);

		/* Instantiate the calculator. */
        PrayerTimeCalc calculator = new PrayerTimeCalc(location, getMethod());

        PrayerTimes pts = calculator.getPrayerTimes(date);
        for (int i = 0; i < 6; i++) {
            pts.get(i).setDate(date);
        }

        return pts;
    }

    public static PrayerTime getNearPray() {

        PrayerTimes todayPT = getPrayerTimes(new Date());

        ArrayList<PrayerTime> times = new ArrayList<>();
        ArrayList<Long> milis = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            if (i != 1) {
                PrayerTime pt = todayPT.get(i);
                pt.setName(TimeNames.getInstance(Locale.getDefault()).get(i));
                milis.add(ptToMilis(pt));
                times.add(pt);
            }
        }

        PrayerTime nextFajr = getFajrNextDay();
        nextFajr.setName("Fajr");
        milis.add(ptToMilis(nextFajr));
        times.add(nextFajr);

        PrayerTime finalPT = null;

        long now = new Date().getTime();
        for (int i = 0; i < milis.size(); i++) {
            if (now < milis.get(i)) {
                finalPT = times.get(i);
                break;
            }
        }

        return finalPT;
    }

    public static PrayerTime getFajrNextDay() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        return getPrayerTimes(cal.getTime()).get(0);
    }

    public static long ptToMilis(PrayerTime pt) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(pt.getDate());

        int hour = pt.getHour();
        int min = pt.getMinute();
        int sec = pt.getSecond();

        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, sec);

        return cal.getTimeInMillis();
    }

    private static LatLng getLatLng() {
        double latitude = -6.1701647;
        double longitude = 106.8292013;

        String curLat = me.getData(Me.CURR_LAT);
        String realLat = me.getData(Me.LAT);

        if (!curLat.isEmpty()) {
            latitude = Double.parseDouble(curLat);
            longitude = Double.parseDouble(me.getData(Me.CURR_LNG));
        } else if (!realLat.isEmpty()) {
            latitude = Double.parseDouble(realLat);
            longitude = Double.parseDouble(me.getData(Me.LNG));
        }

        return new LatLng(latitude, longitude);
    }

    private static Method getMethod() {
        Method method;

        String prayMethod = me.getPrayMethod();
        if (prayMethod.equalsIgnoreCase(Me.EGYPT)) method = Method.EGYPT_SURVEY;
        else if (prayMethod.equalsIgnoreCase(Me.MWL)) method = Method.MUSLIM_LEAGUE;
        else method = Method.NORTH_AMERICA;

        return method;
    }

}

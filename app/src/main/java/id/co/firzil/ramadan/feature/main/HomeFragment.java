package id.co.firzil.ramadan.feature.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.base.BaseFragment;
import id.co.firzil.ramadan.base.RecyclerTouchListener;
import id.co.firzil.ramadan.base.SpacesItemDecoration;
import id.co.firzil.ramadan.feature.article.ArticleFragment;
import id.co.firzil.ramadan.helper.utils.FLog;
import id.co.firzil.ramadan.model.Banner;
import id.co.firzil.ramadan.model.Category;
import io.github.didikk.slider.library.Animations.DescriptionAnimation;
import io.github.didikk.slider.library.SliderLayout;
import io.github.didikk.slider.library.SliderTypes.BaseSliderView;
import io.github.didikk.slider.library.SliderTypes.TextSliderView;
import io.github.didikk.slider.library.Tricks.ViewPagerEx;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {


    //@BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.slider) SliderLayout imageSlider;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showHomeButton(false);

        initSlider();
        initRecycler();
    }

    private void initSlider() {
        /*HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hannibal", R.drawable.hannibal);
        file_maps.put("Big Bang Theory", R.drawable.bigbang);
        file_maps.put("House of Cards", R.drawable.house);
        file_maps.put("Game of Thrones", R.drawable.game_of_thrones);*/

        HashMap<String, String> url_maps = new HashMap<>();
        ArrayList<Banner> banners = Banner.get();
        if (banners.size() > 0) {
            for (Banner banner : banners) url_maps.put(banner.getName(), banner.getImage());

            for (String name : url_maps.keySet()) {
                TextSliderView textSliderView = new TextSliderView(mContext);
                // initialize a SliderLayout
                textSliderView
                        .description(name)
                        .image(url_maps.get(name))
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .setOnSliderClickListener(this);

                //add your extra information
                textSliderView.bundle(new Bundle());
                textSliderView.getBundle()
                        .putString("extra", name);

                imageSlider.addSlider(textSliderView);
            }
            imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
            imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            imageSlider.setCustomAnimation(new DescriptionAnimation());
            imageSlider.setDuration(3000);
            imageSlider.addOnPageChangeListener(this);
        } else imageSlider.setVisibility(View.GONE);
    }

    private void initRecycler() {

        final ArrayList<Category> categories = Category.get();
        GalleryAdapter mAdapter = new GalleryAdapter(mContext, categories);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 3);
        recyclerView.setLayoutManager(mLayoutManager);
        SpacesItemDecoration itemDecoration = new SpacesItemDecoration(mContext, R.dimen.item_offset);
        //recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(mContext, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //toast(categories.get(position).getName());

                Bundle bundle = new Bundle();
                bundle.putInt("data", categories.get(position).getUid());

                Fragment fragment = new ArticleFragment();
                fragment.setArguments(bundle);
                startFragment(fragment);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private ArrayList<Category> generateFound() {
        ArrayList<Category> categories = new ArrayList<>();
        categories.add(new Category("Yayasan Ibnu Umar", ""));
        categories.add(new Category("Dakwah Online mulim.or.id", ""));
        categories.add(new Category("Dakwah online salam dakwah", ""));
        categories.add(new Category("Kajian Ustadz Ammi Nur Baits", ""));
        categories.add(new Category("Yayasan Ibnu Umar", ""));
        categories.add(new Category("Dakwah Online mulim.or.id", ""));
        categories.add(new Category("Dakwah online salam dakwah", ""));
        categories.add(new Category("Kajian Ustadz Ammi Nur Baits", ""));
        categories.add(new Category("Yayasan Ibnu Umar", ""));
        categories.add(new Category("Dakwah Online mulim.or.id", ""));
        categories.add(new Category("Dakwah online salam dakwah", ""));
        categories.add(new Category("Kajian Ustadz Ammi Nur Baits", ""));

        return categories;
    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        imageSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        toast(slider.getBundle().get("extra") + "");
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        FLog.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}

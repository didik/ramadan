package id.co.firzil.ramadan.feature.article;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.co.firzil.ramadan.R;
import id.co.firzil.ramadan.model.Article;

/**
 * Created by didik on 21/05/16.
 * Adapter
 */
public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder> {

    private List<Article> list;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, created;
        public ImageView pic;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_title);
            created = (TextView) view.findViewById(R.id.tv_created);
            pic = (ImageView) view.findViewById(R.id.img_picture);
        }
    }

    public ArticleAdapter(Context mContext, List<Article> list) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.donasi_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Article article = list.get(position);

        String image = article.getImage();
        if (image.isEmpty()) Picasso.with(mContext).load(R.drawable.sample_photo).into(holder.pic);
        else Picasso.with(mContext).load(image).into(holder.pic);
        holder.title.setText(article.getTitle());
        holder.created.setText(article.getCreated());

        ViewCompat.setTransitionName(holder.pic, String.valueOf(position) + "_image");
        ViewCompat.setTransitionName(holder.title, String.valueOf(position) + "_title");
        ViewCompat.setTransitionName(holder.created, String.valueOf(position) + "_created");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
package id.co.firzil.ramadan.helper.handler;

import android.os.Build;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by fahriyalafif on 12/08/2015.
 * Error
 */
public class ErrorWriter {
    private static final String LINE_SEPARATOR = "\n";
    public static String getError(Throwable t){
        StringWriter stackTrace = new StringWriter();
        t.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = new StringBuilder();
        errorReport.append("************ CAUSE OF ERROR ************\n\n");
        errorReport.append(stackTrace.toString());
        try {
            final Runtime runtime = Runtime.getRuntime();
            final long freeMemInMB=runtime.freeMemory() / 1048576L;
            errorReport.append("Free Memory: "+freeMemInMB+" MB");
            errorReport.append(LINE_SEPARATOR);
            //http://stackoverflow.com/questions/3170691/how-to-get-current-memory-usage-in-android
        }
        catch (Exception e){
            e.printStackTrace();
        }

        errorReport.append("\n************ DEVICE INFORMATION ***********\n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Id: ");
        errorReport.append(Build.ID);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n************ FIRMWARE ************\n");
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK_INT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(Build.VERSION.RELEASE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Incremental: ");
        errorReport.append(Build.VERSION.INCREMENTAL);
        errorReport.append(LINE_SEPARATOR);

        return errorReport.toString();
    }
}
